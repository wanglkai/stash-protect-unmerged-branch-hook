package com.atlassian.stash.plugin.hooks.protectbranch;

import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestDirection;
import com.atlassian.stash.pull.PullRequestSearchRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestState;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitRefPattern;
import com.atlassian.stash.util.*;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;

/**
 * This hook disallows pushing a branch deletion when the branch is part of
 * an active (not merged/declined) branch.
 */
public class ProtectUnmergedBranchHook implements PreReceiveRepositoryHook {

    private static final String UNABLE_TO_DELETE_BRANCH_TEMPLATE_KEY = "stash.plugins.hooks.protect.unmerged.branch.unable.to.delete.template";
    public static final Predicate<RefChange> ALLOW_DELETE_REF_CHANGES = new Predicate<RefChange>() {
        @Override
        public boolean apply(RefChange refChange) {
            return RefChangeType.DELETE.equals(refChange.getType());
        }
    };

    private final PullRequestService pullRequestService;
    private final I18nService i18nService;
    private final NavBuilder navBuilder;

    public ProtectUnmergedBranchHook(PullRequestService pullRequestService, I18nService i18nService, NavBuilder navBuilder) {
        this.pullRequestService = pullRequestService;
        this.i18nService = i18nService;
        this.navBuilder = navBuilder;
    }

    @Override
    public boolean onReceive(@Nonnull final RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse hookResponse) {
        Iterable<RefChange> refDeletes = Iterables.filter(refChanges, ALLOW_DELETE_REF_CHANGES);

        boolean hasActiveBranchDeletes = false;
        for (RefChange refDelete : refDeletes) {
            Iterator<PullRequest> pullRequests = listActivePullRequestsForBranch(context.getRepository(), refDelete.getRefId()).iterator();
            if (pullRequests.hasNext()) {
                if (!hasActiveBranchDeletes) {
                    hasActiveBranchDeletes = true;
                    printDashes(hookResponse);
                }
                formatActiveBranchDeletes(hookResponse.err(), refDelete.getRefId(), pullRequests);
            }
        }
        if (hasActiveBranchDeletes) {
            printDashes(hookResponse);
        }

        return !hasActiveBranchDeletes;
    }

    private void printDashes(HookResponse hookResponse) {
        hookResponse.err().println("-----------------------------------------------------");
    }

    private void formatActiveBranchDeletes(PrintWriter printWriter, String refId, Iterator<PullRequest> pullRequests) {
        String unqualifiedRefId = GitRefPattern.HEADS.unqualify(refId);
        printWriter.println(i18nService.getText(UNABLE_TO_DELETE_BRANCH_TEMPLATE_KEY, "Unable to delete branch ''{0}'' because it is involved in the following pull requests:", unqualifiedRefId));
        while (pullRequests.hasNext()) {
            PullRequest pullRequest = pullRequests.next();
            Repository targetRepository = pullRequest.getToRef().getRepository();
            //noinspection ConstantConditions
            printWriter.println("\t" + navBuilder.repo(targetRepository).pullRequest(pullRequest.getId()).buildAbsolute());
        }
    }

    private Iterable<PullRequest> listActivePullRequestsForBranch(Repository repository, String branchRefId) {
        return Iterables.concat(findActivePullRequestsInDirection(repository, branchRefId, PullRequestDirection.INCOMING),
                                findActivePullRequestsInDirection(repository, branchRefId, PullRequestDirection.OUTGOING));
    }

    private Iterable<PullRequest> findActivePullRequestsInDirection(final Repository repository, final String branchRefId, final PullRequestDirection direction) {
        return new PagedIterable<PullRequest>(new PageProvider<PullRequest>() {
            @Override
            public Page<PullRequest> get(PageRequest pageRequest) {
                //noinspection ConstantConditions
                PullRequestSearchRequest request = new PullRequestSearchRequest.Builder()
                    .repositoryAndBranch(direction, repository.getId(), branchRefId)
                    .state(PullRequestState.OPEN)
                    .build();
                return pullRequestService.search(request, pageRequest);
            }
        }, new PageRequestImpl(0, 50));
    }
}
