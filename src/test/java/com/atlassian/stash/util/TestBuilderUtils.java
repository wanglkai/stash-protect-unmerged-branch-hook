package com.atlassian.stash.util;

import java.lang.reflect.Field;

public class TestBuilderUtils {
    /**
     * Some of the fields we're interested in setting in the builders have no setters, this gets around that.
     */
    public static void setPrivateField(Object instance, String fieldName, Object value) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Unable to set " + fieldName, e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Unable to set " + fieldName, e);
        }
    }
}
