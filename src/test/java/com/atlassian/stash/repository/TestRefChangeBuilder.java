package com.atlassian.stash.repository;

public class TestRefChangeBuilder {

    private final SimpleRefChange.Builder builder;

    public TestRefChangeBuilder() {
        builder = new SimpleRefChange.Builder();
        builder.refId("branch-name").fromHash("00000537").toHash("00000837").type(RefChangeType.UPDATE);
    }

    public TestRefChangeBuilder refId(String refId) {
        builder.refId(refId);
        return this;
    }

    public TestRefChangeBuilder type(RefChangeType type) {
        builder.type(type);
        return this;
    }

    public TestRefChangeBuilder fromHash(String hash) {
        builder.fromHash(hash);
        return this;
    }

    public TestRefChangeBuilder toHash(String hash) {
        builder.toHash(hash);
        return this;
    }

    public RefChange build() {
        return builder.build();
    }
}
